function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
  }
  
  // Приклад використання
  const data = ['hello', 'world', 23, '23', null];
  const filteredData = filterBy(data, 'string');
  
  console.log(filteredData); // [23, null]
  